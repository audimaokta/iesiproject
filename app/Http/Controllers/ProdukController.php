<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProdukController extends Controller
{
	public function index()
	{
    		// mengambil data dari table
		$produk = DB::table('produk')->paginate(8);

    		// mengirim data ke view index
		return view('Search_Produk',['produk' => $produk]);

	}

	public function search(Request $request)
	{
		// menangkap data pencarian
		$search = $request->search;

    		// mengambil data dari table 
		$produk = DB::table('produk')
		->where('NAMA_PRODUK','like',"%".$search."%")
		->paginate();

    		// mengirim data ke view index
		return view('Search_Produk',['produk' => $produk]);

	}

}