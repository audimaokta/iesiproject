@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('Anda berhasil masuk!') }}


                    <td><a href="{{ url('produk')}}" class="btn">Klik disini untuk melihat produk</a></td>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
